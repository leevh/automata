#!/usr/bin/env bash

# RUN FROM PROJECT ROOT NOT DOCROOT


echo "Please enter live url (mysite.com): "
read input_url

fin project remove

sudo rm docroot/sites/default/settings.php
sudo cp .docksal/settings_files/settings.php docroot/sites/default/settings.php

fin init

cd docroot
fin drush sql-drop && fin drush -l "http://$input_url/" sql-sync @automata.live @automata.dev
fin drush core-rsync @automata.live:%files/ @automata.dev:%files
fin drush cr

