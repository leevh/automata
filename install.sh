#!/usr/bin/env bash

# Abort if anything fails
#set -e

echo "Please enter dev url (mysite): "
read input_url

# Use the virtual host line
sed -i -e "s/#VIRTUAL_HOST=/VIRTUAL_HOST=/g" .docksal/docksal.env
sed -i -e "s/drupal8/$input_url/g" .docksal/docksal.env
sed -i -e "s/web/web/g" .docksal/docksal.env

echo "Which server will it be on? (leevdesigns or creatria): "
read input_server

# Get the docksal settings files
#mkdir .docksal/settings_files
#cp docroot/sites/default/default.settings.local.php .docksal/settings_files/default.settings.local.php
#cp docroot/sites/default/settings.php .docksal/settings_files/settings.php

#rm -rf docroot
# rm -rf drush
rm -rf .git

echo "Installing drupal via drupal recommended project..."
composer create-project drupal/recommended-project:^8 drupal --no-interaction
cd drupal
echo "LVH: Adding composer dependencies..."
composer require --dev drush/drush:^9
composer require cweagans/composer-patches
cd ..

cp -r drupal/* .
rm -rf drupal
#cp .docksal/settings/* web/sites/default/
mkdir web/themes/custom
echo "Custom themes go here." > web/themes/custom/README.txt


# setup drush aliases
sed -i -e "s/NEWSITE/$input_url/g" drush/sites/NEWSITE.site.yml
sed -i -e "s/SERVER/$input_server/g" drush/sites/NEWSITE.site.yml
mv drush/sites/NEWSITE.site.yml drush/sites/"$input_url".site.yml

#update readme and reinstall script
sed -i -e "s/NEWSITE/$input_url/g" README.md
sed -i -e "s/SERVER/$input_server/g" README.md
sed -i -e "s/NEWSITE/$input_url/g" reinstall.sh

mkdir -p ./web/modules/custom

sed -i -e "s/lvd/$input_url/g" lvd_deploy/lvd_deploy.info.yml
sed -i -e "s/lvd/$input_url/g" lvd_deploy/lvd_deploy.install
sed -i -e "s/lvd/$input_url/g" lvd_deploy/lvd_deploy.module
mv lvd_deploy/lvd_deploy.info.yml lvd_deploy/"$input_url"_deploy.info.yml
mv lvd_deploy/lvd_deploy.install lvd_deploy/"$input_url"_deploy.install
mv lvd_deploy/lvd_deploy.module lvd_deploy/"$input_url"_deploy.module
mv lvd_deploy web/modules/custom/"$input_url"_deploy

git init
git add .
git commit -m "initial commit" --quiet
#git checkout -b "develop"

fin init