# Lee's Drupal 8 starterkit

1.  Clone this repo: `git clone -b recommended-project-8 git@gitlab.com:leevh/drupal.git automata`

2.  cd automata and run `./install.sh`  This will:
  -  Setup the new local site
  -  Clone https://github.com/yaazkal/BOA-drupal-project.git, move it into the web folder and composer install
  -  Run *fin init* for docksal
  -  Clean up folders and start a fresh git repo with an initial commit


## Remote setup for NEW Project

1.  SSH into BOA user and git clone new codebase into ~/static, `live-automata` and `test-automata`

2. Set permissions from /static:
```
chmod 775 live-automata && chmod 775 live-automata/web && chmod 775 live-automata/web/modules && chmod 775 live-automata/web/modules && chmod -R 775 live-automata/web/sites && chown -R host:www-data live-automata &&
chmod 775 test-automata && chmod 775 test-automata/web && chmod 775 test-automata/web/modules && chmod 775 test-automata/web/modules && chmod -R 775 test-automata/web/sites && chown -R host:www-data test-automata
```
3. Add/verify new platforms.  Specify platform path is at `/web`.

4. Create new site on live platform called `live.automata.leevdesigns.com`


6. add settings to local.settings.php (AS ROOT IN SSH)
```
cd /data/disk/host/static/live-automata/web/sites/live.automata.leevdesigns.com && nano local.settings.php
```


```
 $config_directories['sync'] = '../config/default';

 # File system settings.
 #$config['system.file']['path']['temporary'] = 'sites/live.automata.leevdesigns.com/private/temp';
 $domain = $GLOBALS['_leevdesigns']['HTTP_HOST'];
 $domain_array = explode('.', $domain);
 $env = $domain_array[0];
 $config['system.file']['path']['temporary'] = 'sites/'.$env.'.automata.leevdesigns.com/private/temp';
```


7. CURRENTLY NOT USING: Enable automata_deploy with drush `drush en automata_deploy -y`

8. (ON LIVE ONLY) Use onetime login to change admin username to lvd-admin and secure password

9. Clone live site to test.automata.leevdesigns.com

## Sync up Databases

7. Pull down databases by CD into web, and make sure terminal size is normal
on local:
  - `ssh o1.ftp@o1.server.ca1.leevdesigns.com 'drush @automata.pro sql-dump' > db.sql && fin db import db.sql`
on server:
  - `drush sql-sync @automata.pro @test.automata.server.ca1.leevdesigns.com --create-db`

8. Synchronize config with `drush cex` locally, push to git and pull down on test and live, then `drush cim`, `drush updb`, and `drush cr`

## File Sync

- `fin drush core-rsync @automata.live:%files/ @automata.dev:%files`
- `drush core-rsync --omit-dir-times @automata.pro:%files/ @test.automata.server.ca1.leevdesigns.com:%files`


## Local DB operations

- `fin drush sql-dump > ../backup-DATE.sql`


## GO LIVE

- In live site on boa, change local.settings.php to:

```
 $config_directories['sync'] = '../config/default';
 # File system settings.
 $config['system.file']['path']['temporary'] = 'sites/LIVEDOMAIN.COM/private/temp';
```

- Change URI to live domain in drush alias
- Change to live domain in this document for future copy and pasting

## Other Information

- `drush sa` to see aliases
- `to run update db on BOA, use drush @site-name updb`
- For full proper git access on platforms, login with root and `su -s /bin/bash - host`
- 403 permissions errors in docksal? try `find . -type d -exec chmod -R a+rx '{}' \;` in the project root
- problems with available updates page? try `delete from key_value where collection='update_fetch_task'`
- Mismatched entity and/or field definitions?  `drush entity-updates`

## Remote setup for EXISTING Projects

TODO
